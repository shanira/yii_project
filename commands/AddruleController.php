<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{
	public function actionOwntask()
	{ 
		$auth = Yii::$app->authManager; 
		$rule = new \app\rbac\OwnTaskRule;
		$auth->add($rule);
	}
}