<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property string $owner
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'owner'], 'string', 'max' => 255],
			[['name', 'owner',], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'owner' => 'Owner',
        ];
    }
	public static function getProject()
	{
		$allProject = self::find()->all();
		$allProjectArray = ArrayHelper::
					map($allProject, 'id', 'name');
		return $allProjectArray;						
	}
	public function getOwnerItem()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }	
	
}
