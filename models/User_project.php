<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_project".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_project
 */
class User_project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_project'], 'required'],
            [['id', 'id_user', 'id_project'], 'integer'],
			[['id_user', 'id_project'], 'unique', 'targetAttribute' => ['id_user', 'id_project'], 'message' => 'The user is already assigned to this project'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'User',
            'id_project' => 'Project',
        ];
    }
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }	
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }
	
			
}
