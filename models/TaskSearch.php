<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'body', 'status', 'priority', 'responsibleDepartment', 'requierdFinishDate', 'actualFinishDate', 'created_at', 'updated_at', 'created_by', 'updated_by', 'project'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $team_tasks = null, $own_tasks = null)
    {
        $query = Task::find();
		
		if($team_tasks != null){
			$query = Task::find()->joinWith('projectItem')->where(['owner'=>$team_tasks]);
		}
		
		if($own_tasks != null){
			$query = Task::find()->innerJoin('user_task','task.id = user_task.id_task')->where(['id_user'=>$own_tasks]);
	
	}
		

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'requierdFinishDate' => $this->requierdFinishDate,
            'actualFinishDate' => $this->actualFinishDate,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'priority', $this->priority])
            ->andFilterWhere(['like', 'responsibleDepartment', $this->responsibleDepartment])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
			->andFilterWhere(['like', 'project', $this->project]);

        return $dataProvider;
    }
}
