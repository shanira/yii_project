<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\Status;
use app\models\Priority;
use app\models\Project;
 use yii\db\ActiveRecord;
 use yii\behaviors\TimestampBehavior;
 use yii\db\Expression;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $status
 * @property string $priority
 * @property string $responsibleDepartment
 * @property string $requierdFinishDate
 * @property string $actualFinishDate
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requierdFinishDate', 'actualFinishDate', 'created_at', 'updated_at'], 'safe'],
            [['title', 'body', 'status', 'priority', 'responsibleDepartment', 'created_by', 'updated_by', 'project'], 'string', 'max' => 255],
			[['requierdFinishDate','title', 'body','status','priority','project'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'status' => 'Status',
            'priority' => 'Priority',
            'responsibleDepartment' => 'Department',
            'requierdFinishDate' => 'Requierd Finish Date',
            'actualFinishDate' => 'Actual Finish Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
			'project' => 'Project',
        ];
    }
	
/* 	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    } */
	
	public function beforeSave($insert) {
 
		if(parent::beforeSave($insert)) {
		
			/* if($this->hasAttribute("id") && $insert && !Yii::$app->user->isGuest )
				$this->id = Yii::$app->user->id; */
					
			if($this->hasAttribute("created_at") && $insert)
				$this->created_at = new Expression("now()");
		
			if($this->hasAttribute("created_by") && $insert && !Yii::$app->user->isGuest)
				$this->created_by = Yii::$app->user->id;
			
			if($this->hasAttribute("updated_at"))
				$this->updated_at = new Expression("now()");
		
			if($this->hasAttribute("updated_by") && !Yii::$app->user->isGuest)
				$this->updated_by = Yii::$app->user->id;
		
			return true;
		
			}
					
	}
	

	/* public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                //'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                ],
             
        ];
     }
 */
	public function getCreatedByItem()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdatedByItem()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
   
    }	
	public function getPriorityItem()
    {
        return $this->hasOne(Priority::className(), ['id' => 'priority']);
    }	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
	}	
		public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'name']);
    	
    }

		public function getUsertask1()
    {
        return $this->hasMany(User_task::className(), ['id_task' => 'id']);
	}	

}
