<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "priority".
 *
 * @property integer $id
 * @property string $name
 */
class Priority extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'priority';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
 	public static function getPriorities()
	{
		$allPriorities = self::find()->all();
		$allPrioritiesArray = ArrayHelper::
					map($allPriorities, 'id', 'name');
		return $allPrioritiesArray;						
	}
	public static function getPrioritiesWithAllPriorities()
	{
		$allPriorities = self::getPriorities();
		$allPriorities[null] = 'All Statuses';
		$allPriorities = array_reverse ( $allPriorities, true );
		return $allPriorities;	
	}	
}
