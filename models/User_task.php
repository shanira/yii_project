<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_task".
 *
 * @property integer $id
 * @property integer $id_task
 * @property integer $id_user
 */
class User_task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_task', 'id_user'], 'required'],
            [['id_task', 'id_user'], 'integer'],
			[['id_user', 'id_task'], 'unique', 'targetAttribute' => ['id_user', 'id_task'], 'message' => 'The user is already assigned to this task'],
        
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_task' => 'task',
            'id_user' => 'user',
        ];
    }
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }	
	
	public function getTaskItem()
    {
        return $this->hasOne(Task::className(), ['id' => 'id_task']);
    }
	
}
