<?php

namespace app\controllers;

use Yii;
use app\models\User_project;
use app\models\User;
use app\models\User_projectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;


/**
 * User_projectController implements the CRUD actions for User_project model.
 */
class UserprojectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User_project models.
     * @return mixed
     */
    public function actionIndex($id)
    {
		if (!\Yii::$app->user->can('can_ProjectManager')&&!\Yii::$app->user->can('can_ceo'))
			throw new UnauthorizedHttpException ('You are not allowed to access this page');
        	
        $searchModel = new User_projectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

		
		$allUsers = Yii::$app->db->createCommand('SELECT * FROM user 
			where `id` not in (
			select id_user 
			from user_project 
			where id_project = '.$id.')'

			);
			$allUsers = Yii::$app->db->createCommand('SELECT * FROM user') 
            ->queryAll();
			//$allUsers = User::find()->all();
		$allUsersArray = ArrayHelper::
					map($allUsers, 'id', 'name');
			
				
			
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'project'=>$id,
			'allUsersArray'=>$allUsersArray,
        ]);
    }

    /**
     * Displays a single User_project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		if (!\Yii::$app->user->can('can_ProjectManager')&&!\Yii::$app->user->can('can_ceo'))
			throw new UnauthorizedHttpException ('You are not allowed to access this page');
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User_project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project)
    {
		if (!\Yii::$app->user->can('can_ProjectManager'))
			throw new UnauthorizedHttpException ('You are not allowed to access this page');
        
        $model = new User_project(['id_project'=>$project]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id_project]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User_project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		if (!\Yii::$app->user->can('can_ProjectManager'))
			throw new UnauthorizedHttpException ('You are not allowed to access this page');
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User_project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('can_ProjectManager'))
			throw new UnauthorizedHttpException ('You are not allowed to access this page');
        
		$myid = $this->findModel($id)->id_project;
        $this->findModel($id)->delete();

         return $this->redirect(['index', 'id' => $myid]);
    }

    /**
     * Finds the User_project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User_project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User_project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
