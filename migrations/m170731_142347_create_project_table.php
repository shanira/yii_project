<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170731_142347_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
			'name' => $this->String(),
			'owner' => $this->String(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
