<?php

use yii\db\Migration;

/**
 * Handles adding email to table `user`.
 */
class m170803_163524_add_email_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'email', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'email');
    }
}
