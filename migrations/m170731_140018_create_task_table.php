<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m170731_140018_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
			'title' => $this->string(),
			'body' => $this->string(),
			'status' => $this->string(),
			'priority' => $this->string(),
			'responsibleDepartment' => $this->string(),
			'requierdFinishDate' => $this->date(),
			'actualFinishDate' => $this->date(),
			'created_at' => $this->date(),
            'updated_at' => $this->date(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task');
    }
}
