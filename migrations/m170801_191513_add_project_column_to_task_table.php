<?php

use yii\db\Migration;

/**
 * Handles adding project to table `task`.
 */
class m170801_191513_add_project_column_to_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('task', 'project', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('task', 'project');
    }
}
