<?php

use yii\db\Migration;

/**
 * Handles the creation of table `priority`.
 */
class m170731_183944_create_priority_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('priority', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('priority');
    }
}
