<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Task;
/* @var $this yii\web\View */
/* @var $searchModel app\models\User_taskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$task = Task::find()->where(['id'=>$_GET['id']])->all();
$this->title = $task[0]->title;
//$this->title = 'Task Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-task-index">

    <h1><?= Html::encode($this->title) ?></h1><h3>Task Members</h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
	<?php if (\Yii::$app->user->can('can_projectManager')){ ?>
        <?= Html::a('Add Task Members', ['create','task'=>$_GET['id']], ['class' => 'btn btn-success']) ?>
			<?php } ?>
		<?= Html::a('Back to task page', ['task/view','id'=>$_GET['id']], ['class' => 'btn btn']) ?>
   
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            [
				'attribute' => 'id_user',
				'label' => 'Member',
				'format' => 'raw',
				'value' => function($model){
							//return ($model->ownerItem->name);
							 return Html::a($model->userItem->name, ['user/view', 'id' => $model->userItem->id]);
					},		
				/* 'value' => function($model){
							 return Html:: a($model->authorItem->name);
					},		 */
			],
           // 'id_project',
			['class' => ActionColumn::className(),'template'=>'{delete} ' ]
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
        
    ]); ?>
</div>
