<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Task;
use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User_task */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
//$tasks = Task::find()->where(['id'=>$_GET['task']])->all();
$task = Task::find()->where(['id'=>$_GET['task']])->one();
//$task = $tasks[0];
//echo $task->projectItem->id;
 $allUsers = Yii::$app->db->createCommand('SELECT * FROM user 
			where `id`  in (
			SELECT `id_user`  
			FROM `user_project`
			WHERE `id_project` = '.$task->projectItem->id.')
			and `id`  not in (
			select id_user 
			from user_task 
			where id_task = '.$_GET['task'].')'

			
			
			)
			
		
            ->queryAll(); 
			
		$allUsersArray = ArrayHelper::
					map($allUsers, 'id', 'name');


?>
<div class="user-task-form">

    <?php $form = ActiveForm::begin(); ?>

 
   <?= $form->field($model,  'id_user')->dropDownList($allUsersArray) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		
		<?= Html::a('Cancel', ['/usertask/index','id'=>$_GET['task']], ['class' => 'btn btn-warning']) ?>
         </div>

    <?php ActiveForm::end(); ?>

</div>
