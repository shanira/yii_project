
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([

				
                'brandLabel' => '<img src="./images/logo2.png" class="img-responsive"/> ',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => ' navbar-default navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
				
				
            ];
            if (Yii::$app->user->isGuest) {
     
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
				
            } else {
				 //$menuItems[] = ['label' => 'Users','url' => ['/user'],'visible'=> true];
				 $menuItems[] = ['label' => 'Projects','url' => ['/project'],'visible'=> true];
				 $menuItems[] = ['label' => 'Tasks','url' => ['/task'],'visible'=> true];
                 if  (\Yii::$app->user->can('can_ProjectManager')||\Yii::$app->user->can('can_ceo')){
					$menuItems[] = ['label' => 'Users','url' => ['/user'],'visible'=> true];
				}
				 $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']];
					
               // ];
            }
			
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
	
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Itzhak Grinboim, Eliya Nagar ,Shani Rabi-Guttel</p>

        <p class="pull-right"> פרוייקט במסגרת קורס ניתוח ותכנון מערכות מידע ב <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
