<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if (\Yii::$app->user->can('can_admin')){ ?>
    <p>
    <?= Html::button('Create User', ['value'=>Url::to('index.php?r=user/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>  
	<!--?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
	<?php } ?>
    <?php
	Modal::begin([
	 'header'=> '<h4>New User</h4>',
	 'id'=>'modal',
	 'size'=>'modal-lg',
	]);
		echo "<div id='modelContent'></div>";
	Modal::end();
	?>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        //    ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'name',
            'username',
         //   'password',
			'email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
