<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Project;
/* @var $this yii\web\View */
/* @var $searchModel app\models\User_projectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$task = Project::find()->where(['id'=>$_GET['id']])->all();
$this->title = $task[0]->name;

//$this->title = 'Project Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-project-index">
	
    <h1><?= Html::encode($this->title) ?></h1><h3>Project Members</h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
	<?php if (\Yii::$app->user->can('can_projectManager')){ ?>
        <?= Html::a('Add Project Member', ['create','project'=>$project], ['class' => 'btn btn-success']) ?>
	<?php } ?>
		<?= Html::a('Back to project page', ['project/view','id'=>$_GET['id']], ['class' => 'btn btn']) ?>
   
	</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

          //  'id',
           // 'id_user',
			 [
				'attribute' => 'id_user',
				'label' => 'Member',
				'format' => 'raw',
				'value' => function($model){
							//return ($model->ownerItem->name);
							 return Html::a($model->userItem->name, ['user/view', 'id' => $model->userItem->id]);
					},		
				/* 'value' => function($model){
							 return Html:: a($model->authorItem->name);
					},		 */
			],
           // 'id_project',
			['class' => ActionColumn::className(),'template'=>'{delete} ' ]
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
