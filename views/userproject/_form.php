<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\User_project */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

$allUsers = Yii::$app->db->createCommand('SELECT * FROM user 
			where `id` not in (
			select id_user 
			from user_project 
			where id_project = '.$_GET['project'].')'

			)
		
            ->queryAll();
			
		$allUsersArray = ArrayHelper::
					map($allUsers, 'id', 'name');


?>
<div class="user-project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php ///$form->field($model, 'id')->textInput() ?>

   
	 <?= $form->field($model,  'id_user')->dropDownList($allUsersArray) ?>

    <?php //$form->field($model, 'id_project')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		
		<?= Html::a('Cancel', ['/userproject/index','id'=>$_GET['project']], ['class' => 'btn btn-warning']) ?>
      
	</div>

    <?php ActiveForm::end(); ?>

</div>
