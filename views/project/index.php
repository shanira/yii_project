<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

		<?php if (\Yii::$app->user->can('can_admin')){ ?>
    <p>
	 <?= Html::button('Create Project', ['value'=>Url::to('index.php?r=project/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>  
        <!--?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
		<?php }	?>
	 <?php
	Modal::begin([
	 'header'=> '<h4>New Project</h4>',
	 'id'=>'modal',
	 'size'=>'modal-lg',
	]);
		echo "<div id='modelContent'></div>";
	Modal::end();
	?>
    <?= GridView::widget([
       'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            //'id',
           // 'name',
			[
				'attribute' => 'name',
				'label' => 'Project Name',
				'format' => 'raw',
				'value' => function($model){
							//return ($model->ownerItem->name);
							 return Html::a($model->name, ['view', 'id' => $model->id]);
					},		
				/* 'value' => function($model){
							 return Html:: a($model->authorItem->name);
					},		 */
			],
            //'owner',
			 [
				'attribute' => 'owner',
				'label' => 'Owner',
				'format' => 'raw',
				'value' => function($model){
							//return ($model->ownerItem->name);
							 return Html::a($model->ownerItem->name, ['user/view', 'id' => $model->ownerItem->id]);
					},		
				/* 'value' => function($model){
							 return Html:: a($model->authorItem->name);
					},		 */
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
