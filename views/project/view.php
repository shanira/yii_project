<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	<?php if (\Yii::$app->user->can('can_admin')){ ?>
			
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
	<?php }	?>
		<?= Html::a('Project Members', ['/userproject/index','id' => $model->id], ['class' => 'btn btn-info']) ?>
      
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
       //     'id',
	    
          
            [
            'label'  =>   'Project Name',
            'value'  => $model->name,
           ],
			 [
            'label'  => 'Owner',
            'value'  => $model->ownerItem->name,
           
        ],
        ],
    ]) ?>

</div>
