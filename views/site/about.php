<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    "Software-Tech" is the largest and the leading IT services group in Israel, provides a comprehensive range of computer services in the field of information technology. It is known for maintaining quality standards, customer service, and professionalism at an international level. The group combines innovation and decades of experience in setting up, implementing and integrating information systems. Its more than 3000 experts in a variety of information technologies are deployed throughout Israel at the facilities of the company and its customers. MalamTeam offers distinctive integrated solutions for all types of hardware, software, database and communications platforms; each of its business units specializes in a distinctive area.
    </p>

    
</div>
