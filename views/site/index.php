
<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


$this->title = 'To-Do Bom';
?>
<div class="site-index">
<?php if (Yii::$app->user->isGuest) {?>
<div class="jumbotron">
    <h1>Welcome!</h1
	
 
    <div class="thumbnail">
	 <h3></h3>
      <!--img src="./images/login2.png" -->
      <div class="caption">

        <p><a href="http://shanira.myweb.jce.ac.il/30049/yii_project/basic/web/index.php?r=site%2Flogin" class="btn btn-primary" role="button">Please Login <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> 
	
   
  </div>

  </div>
   </div>
<?php }else{ ?>
 <h1>Welcome To Project Managment Site!</h1>
<?php
//Calculations:

if (\Yii::$app->user->can('can_ceo')){ 
	$ownProjects = Yii::$app->db->createCommand('SELECT count(*) from project')
		 ->queryScalar();

	$teamTasks = Yii::$app->db->createCommand('SELECT count(*) from task where status!=3')
		 ->queryScalar();			 
	$projectLabel = "Projects";
	$taemtasksLabel	= "Tasks";
	$tasksLabel = "";
}else{
$ownProjects = Yii::$app->db->createCommand(' 
			SELECT count(*) FROM `project` WHERE `owner` = ' . Yii::$app->user->getId()
			)
		 ->queryScalar();

$teamTasks = Yii::$app->db->createCommand(' 
			SELECT count(*) FROM `task` inner join project p ON `project` = p.id WHERE `project` in ( select id from project where owner = '.		 

			 Yii::$app->user->getId() .')'
			)
		 ->queryScalar();			 
		 
$ownTasks = Yii::$app->db->createCommand(' 
			SELECT count(*) FROM `user_task` WHERE `id_user` =' . Yii::$app->user->getId()
			)
		 ->queryScalar();		 
		
			$projectLabel = "My Projects";
	$taemtasksLabel	= "Team Tasks";
	$tasksLabel = "My Tasks";
		 }

?>
<br>
	
    <div class="body-content">

			<div class="alert alert-info">
			<div class="row">
				<div class="round-button col-sm-4"><div class="round-button-circle"><a href="/30049/yii_project/basic/web/index.php?r=project/index&owner_projects=<?php echo Yii::$app->user->getId();?>" class="round-button"><?= $ownProjects ?></a></div></div>
			
				<div class="col-sm-8"><h1>&nbsp; <?= $projectLabel ?></h1></div>
			</div>
			</div>
			
			<div class="alert alert-success">
				<div class="row">
					<div class="round-button col-sm-4"><div class="round-button-circle"><a href="/30049/yii_project/basic/web/index.php?r=task/index&team_tasks=<?php echo Yii::$app->user->getId();?>" class="round-button"><?=$teamTasks?></a></div></div>
				
					<div class="col-sm-8"><h1>&nbsp; <?=$taemtasksLabel?></h1></div>
				</div>
			</div>
			
			<div class="alert alert-warning">
				<div class="row">
					<div class="round-button col-sm-4"><div class="round-button-circle"><a href="/30049/yii_project/basic/web/index.php?r=task/index&own_tasks=<?php echo Yii::$app->user->getId();?>" class="round-button"><?=$ownTasks ?></a></div></div>
				
					<div class="col-sm-8"><h1>&nbsp; <?=$tasksLabel?></h1></div>
				</div>
			</div>
	
	
	
     </div>

</div>

    </div>
</div>
<style>
.round-button {
	width:17%;
}
.round-button-circle {
	width: 130%;
	height:0;
	padding-bottom: 100%;
    border-radius: 50%;
	border:10px solid #cfdcec;
    overflow:hidden;
    
    background: #4679BD; 
    box-shadow: 0 0 3px gray;
}
.round-button-circle:hover {
	background:#30588e;
}
.round-button a {
    display:block;
	float:left;
	width:100%;
	padding-top:50%;
    padding-bottom:50%;
	line-height:1em;
	margin-top:-0.5em;
    
	text-align:center;
	color:#e2eaf3;
    font-family:Verdana;
    font-size:1.2em;
    font-weight:bold;
    text-decoration:none;
}
.alert {
   width:50%;    
}

<?php
if (!\Yii::$app->user->can('can_ProjectManager')&&!\Yii::$app->user->can('can_ceo')){ ?>
.alert-info{
	display:none;
}	

.alert-success{
	display:none;
}	
    
    
<?php }
if (\Yii::$app->user->can('can_ceo')){
?>
.alert-warning{
	display:none;
}	
    
    
<?php } ?>
</style>
<div class="jumbotron">
 <!--  
<div class="row">
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
	 <h3>Projects</h3>
      <img src="./images/Project.png" alt="...">
      <div class="caption">
        <p>...</p>
        <p><a href="http://shanira.myweb.jce.ac.il/30049/yii_project/basic/web/index.php?r=project" class="btn btn-primary" role="button">VIew <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> 
	
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-6">
   <div class="thumbnail">
   <h3>Tasks</h3>
      <img src="./images/Task.png" alt="...">
      <div class="caption">
        
        <p>...</p>
        <p><a href="http://shanira.myweb.jce.ac.il/30049/yii_project/basic/web/index.php?r=task" class="btn btn-primary" role="button">View <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> </p>
      </div>
    </div>
	</div>
						 <!--div class="col-sm-6 col-md-4">
					   <div class="thumbnail">
					   <h3>Assingment</h3>
						  <img src="./images/Assign.png" alt="...">
						  <div class="caption">
							
							<p>...</p>
							<p><a href="/task" class="btn btn-primary" role="button">View <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a> </p>
						  </div>
						</div>
						</div>
</div>
-->
<?php } ?>		

        <!--p class="lead">You have successfully created your Yii-powered application.</p-->

        <!--p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p-->
    </div>
