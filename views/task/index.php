<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
if (\Yii::$app->user->can('can_preformTask')){ 
	$this->title = 'My Tasks';	
} 
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="task-index">
	<div class="row">
		<div class="col-sm-8">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>		
			<div class="col-sm-4">
			<h5><u><i>Line's Colours Legend:</i></u></h5>
				<h5><font color="red">Red Colour: </font>Priority- High & Status-Not Started</h5>
				<h5><font color="yellow">Yellow Colour: </font>Status-In Progress</h5>
				<h5><font color="green">Green Colour: </font>Status-Finished</h5><br>
			</div>	
		</div>	
		
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		<?php 	if (\Yii::$app->user->can('can_ProjectManager')){ ?>
		
			<?= Html::button('Create Task', ['value'=>Url::to('index.php?r=task/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>  
		<?php } ?>
		
        <!--?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
	<?php
	Modal::begin([
	 'header'=> '<h4>New Task</h4>',
	 'id'=>'modal',
	 'size'=>'modal-lg',
	]);
		echo "<div id='modelContent'></div>";
	Modal::end();
	?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'rowOptions' =>function($model){
					if(($model->status == '1')&($model->priority == '1')){//Not Started & High 
						return['class'=>'danger'];
					}else if($model->status == '3'){ //finished task
					return['class'=>'success'];
					}else if($model->status == '2'){ //In progress task
					return['class'=>'warning'];}
					},
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'body',
            [
				'attribute' => 'status',
				'label' => 'status',
				'value' => function($model){
							return $model->statusItem->name;
					},
			//add to filters
				'filter'=>Html::dropDownList('TaskSearch[status]', 
				$status, $statuses, ['class'=>'form-control']),	
			],
            [
				'attribute' => 'priority',
				'label' => 'Priority',
				'value' => function($model){
							return $model->priorityItem->name;
					},
			//add to filters
				'filter'=>Html::dropDownList('TaskSearch[priority]', 
				$priority, $priorities, ['class'=>'form-control']),						
			],
             //'responsibleDepartment',
             'requierdFinishDate',
             'actualFinishDate',
             'created_at',
             'updated_at',
             		   [
				'attribute' => 'created_by',
				'label' => 'Created by',
				'format' => 'raw',
				'value' => function($model){
						
							 return Html::a($model->createdByItem->name, ['user/view', 'id' => $model->createdByItem->id]);
					},		
			],
             		   [
				'attribute' => 'updated_by',
				'label' => 'Updated by',
				'format' => 'raw',
				'value' => function($model){
						
							 return Html::a($model->updatedByItem->name, ['user/view', 'id' => $model->updatedByItem->id]);
					},		
			],
			 [
				'attribute' => 'project',
				'label' => 'Project',
				'format' => 'raw',
				'value' => function($model){
							//return $model->projectItem->name;
							return Html::a($model->projectItem->name, ['project/view', 'id' => $model->projectItem->id]);
					},
			],
            ['class' => 'yii\grid\ActionColumn'],
        ],
		
    ]); ?>

</div>
