<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Priority;
use app\models\Project;
use app\models\Department;
use app\models\User;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">
	
	<?php if(!$model->isNewRecord){ ?>
	<?= Html::a('Task Members', ['/usertask/index','id' => $model->id], ['class' => 'btn btn-info']) ?>
    <br>  </br>
	<?php } ?>
	
    <?php $form = ActiveForm::begin(); ?>
	
	<?php if (\Yii::$app->user->can('can_ProjectManager')){ ?>
	<?= $form->field($model, 'project')->dropDownList(Project::getProject())  ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?>
	<?php } ?>
	
	
    <?= $form->field($model, 'status')->dropDownList(Status::getStatuses()) ?> 
	<?php if (\Yii::$app->user->can('can_ProjectManager')){ ?>
	<?= $form->field($model, 'priority')->dropDownList(Priority::getPriorities()) ?>
    <!--?= $form->field($model, 'priority')->textInput(['maxlength' => true]) ?-->
    <?= $form->field($model, 'responsibleDepartment')->dropDownList(Department::getDepartment()) ?>
	<?= $form->field($model, 'requierdFinishDate')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-m-d'
        ]
	]);?>
    <!--?= $form->field($model, 'requierdFinishDate')->textInput() ?-->
	
	<?php if(!$model->isNewRecord){ ?>
	<?= $form->field($model, 'actualFinishDate')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-m-d'
        ]
	]);?>
		
	<?php }  }?>
	
	
    <!--?= $form->field($model, 'actualFinishDate')->textInput() ?-->

    <!--?= $form->field($model, 'created_at')->textInput() ?-->

    <!--?= $form->field($model, 'updated_at')->textInput() ?-->

    <!--?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?-->
 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
