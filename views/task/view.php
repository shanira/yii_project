<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	<?php if (\Yii::$app->user->can('can_projectManager')){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
			<?php } ?>
		<?= Html::a('Task Members', ['/usertask/index','id' => $model->id], ['class' => 'btn btn-info']) ?>
      
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            'status',
            'priority',
            'responsibleDepartment',
            'requierdFinishDate',
            'actualFinishDate',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
			 'project',
        ],
    ]) ?>

</div>
